#coding: utf-8

import psycopg2




def connect_db(user, password, host, port, database):
    db = psycopg2.connect(user=user, password=password, host=host, port=port, database=database)
    cursor = db.cursor()
    return db, cursor


def queryset(cursor):
    cursor.execute("select * from prueba01;")
    return cursor.fetchall()


def search_id(cursor):
    id = int(input("Escribe el ID: "))
    cursor.execute("select id, name from prueba01 where id = {0};".format(id))
    return cursor.fetchall()


def insert_into(cursor):
    id = int(input("Escribe el ID: "))
    name = str(input("Escribe el Name: "))
    cursor.execute("insert into prueba01 (id, name) values({0}, {1});".format(id, name))
    return cursor

#print("Recomendacion, utilizar comillas simples para escribir los datos.")

print("")
user = "test001" #str(input("Username: "))
password = "test001_user" #str(input("Password: "))
host = "127.0.0.1" #str(input("Host: "))
port = "5432" #str(input("Port: "))
database = "db_test001" #str(input("Database Name: "))

db, cursor = connect_db(user, password, host, port, database)
#print(db)

print(insert_into(cursor))
print(queryset(cursor))

db.commit()
db.close()
