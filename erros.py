#coding: utf-8

a = 70 # sabemos que es un entero
b = "2.5" # sabemos que es un string

#print(a + b) # sabemos que sale un error de operando no soportado

""" 
por lo tanto sabemos que los valores enteros
no se pueden sumar con valores no numericos,
como los strings.
"""

'''
Las comillas simples tambien 
sirven para realizar los 
comentarios multilinea
'''

def hola(arg):
	"""
	Esta funcion sirve para solicitar
	un argumento o parametro al usuario
	despues la funcion regresa un mensaje
	que dice hola y le agrega el argumento.
	"""
	return "Hola: {}".format(arg)

print(hola("Ray"))

input("Presione una tecla para terminar... ")

print("Finalizado..")