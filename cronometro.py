#coding: utf-8
#cronometro.py

"""
Programa para simular el comportamiento de un cronometro
"""
import time

for h in range(24):
	for m in range(60):
		for s in range(60):
			print("{}: {}: {}".format(h, m, s))
			time.sleep(0.5)