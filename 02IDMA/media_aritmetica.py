#coding: utf-8
#media_aritmetica.py

"""
programa para calcular la media aritmetica de una poblacion de datos
"""

import random

P = []

for i in range(100):
	P.append(random.randint(19, 23))

X = 0.0
N = len(P)
_X = 0.0

for xi in P:
	_X += xi

X = _X/N

print("Poblacion: {0}".format(P))
print("")
print("La Longitud de la Poblacion: {0}".format(N))
print("La suma de las xi es: {0}".format(_X))
print("La media de las edades es: {0}".format(X))



