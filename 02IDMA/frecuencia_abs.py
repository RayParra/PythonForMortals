#coding: utf-8
#frecuencia_abs.py

"""
Programa para calcular la frecuencia absoluta de los datos en poblacion
"""
import random

N = []
ma = []

for i in range(100):
	N.append(random.randint(19, 23))

m = 50


for i in range(m):
	ma.append(random.choice(N))


tf = ma

print("Tamaño de la poblacion: {0}".format(len(N)))
print("Poblacion: {0}".format(N))
print("")
print("Tamaño de la muestra aleatoria: {0}".format(len(ma)))
print("Muestra aleatoria: {0}".format(ma))

print("Min: {0}".format(min(ma)))
print("Max: {0}".format(max(ma)))

fa19 = 0
fa20 = 0
fa21 = 0
fa22 = 0
fa23 = 0
for d in ma:
	if d == 19:
		fa19 += 1
	elif d == 20:
		fa20 += 1
	elif d == 21:
		fa21 += 1
	elif d == 22:
		fa22 += 1
	elif d == 23:
		fa23 += 1
	else:
		"Dato no valido."

print("Frecuencia absoluta del 19: {0}".format(fa19))
print("Frecuencia absoluta del 20: {0}".format(fa20))
print("Frecuencia absoluta del 21: {0}".format(fa21))
print("Frecuencia absoluta del 22: {0}".format(fa22))
print("Frecuencia absoluta del 23: {0}".format(fa23))
print("La suma de las frecuencias es: {0}".format(fa19 + fa20 + fa21 + fa22 + fa23))





















