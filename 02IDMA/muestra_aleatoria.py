#coding: utf-8
#muestra_aleatoria.py

"""
programa para crear muestras aleatorias de la poblacion

"""
import random

N = []

for i in range(100):
	N.append(random.randint(18, 25))

ma = [30, 40, 50]

m = []

for j in range(random.choice(ma)):
	m.append(random.choice(N))


print("Poblacion: {0}".format(N))
print("Muestra Aleatoria: {0}".format(m))
print("tamaño de la muestra: {0}".format(len(m)))