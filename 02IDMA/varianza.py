#coding: utf-8
#varianza.py

"""
Programa para calcular la varianza de los datos 

## Rating de la serie Cobra Kai

"""


import random

P = []

var = 0.0


for i in range(100):
	P.append(random.randint(1, 5))

mu = 0.0
N = len(P)
_X = 0.0

for xi in P:
	_X += xi

mu = _X/N


for xi in P:
	_X += pow((xi - mu), 2)

var = _X/N

print("Poblacion: {0}".format(P))
print("Tamaño de la poblacion: {0}".format(N))
print("Media: {0}".format(mu))
print("Varianza: {0}".format(var))













