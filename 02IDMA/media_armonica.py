#coding:utf-8
#media_armonica.py

"""
programa para calcular la media armonica de los datos en una poblacion
"""


import random

P = []

for i in range(100):
	P.append(random.randint(19, 23))




# MEDIA ARMONICA
H = 0.0
n = len(P)
_xi = 0.0


#H = n / (sum((1/xi)))

for xi in P:
	_xi += (1 / float(xi))

H = n / _xi
print("La media armonica es: {0}".format(H))

w = [0.1, 0.2, 0.3, 0.4, 0.5, 0.5, 0.7, 0.8, 0.9]


# MEDIA PONDERADA
# MP = (w1(x1) + w2(x2) +, ..., + wi(xi)) / (w1 + w2 +, ..., + wi)
_wx = 0.0
_wi = 0.0 

for xi in P:
	wi = random.choice(w)
	_wx += ( wi * xi)
	_wi += wi

MP = _wx / _wi


print("La media Ponderada es: {0}".format(MP))


# MEDIA ARITMETICA
X = 0.0
N = len(P)
_X = 0.0

for xi in P:
	_X += xi

X = _X/N

print("La media Artimetca es: {0}".format(X))



