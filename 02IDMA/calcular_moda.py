#coding: utf-8
#calcular_moda.py

"""
Programa para obtener la moda en una poblacion de datos
"""

import random

N = []

for i in range(1000000):
	N.append(random.randint(19, 21))


m19 = 0
m20 = 0
m21 = 0

# m19, m20, m21 = 0

for m in N:
	if m == 19:
		m19 += 1
	elif m == 20:
		m20 += 1
	elif m == 21:
		m21 += 1
	else: 
		print("Valor fuera de Rango")


print("Poblacion: {0}".format(N))
print(" ")
print("El dato 19 aparece {0} veces en la poblacion.".format(m19))
print("El dato 20 aparece {0} veces en la poblacion.".format(m20))
print("El dato 21 aparece {0} veces en la poblacion.".format(m21))

if m19 >= m20 and m19 >= m21:
	print("La moda es: 19")
elif m20 >= m19 and m20 >= m21:
	print("La moda es: 20")
elif m21 >= m19 and m21 >= m20:
	print("La moda es: 21")
else: 
	print("Datos fuera de Rango")