# coding: utf-8
#time.py

"""
Programa que calcula el tiempo en segundos
minutos y horas, basado en el numero
de dias solicitados al usuario
"""

DIA = 24
HORA = 60
MINUTO = 60

numero_dias = int(input("Escribe el numero de dias a calcular: "))

print("El total de horas de {0} dias es: {1} ".format(numero_dias, DIA * numero_dias))
print("El total de minutos de {0} dias es: {1}".format(numero_dias, HORA * DIA * numero_dias))
print("El total de segundos de {0} dias es: {1}".format(numero_dias, MINUTO * HORA * DIA * numero_dias))


## hacer un programa que calcule el numero total de minutos y segundos 
## de las horas que duermo