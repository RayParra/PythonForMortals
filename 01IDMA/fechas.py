#coding:utf-8
#fechas.py

import datetime

print("La Fecha de hoy es: {0}".format(datetime.date.today()))

date = datetime.date.today()

print("El Año actual es: {0}".format(date.year))
print("El Mes actual es: {0}".format(date.month))
print("El Dia actual es: {0}".format(date.day))


today = datetime.date.today()
one_day = datetime.timedelta(days=1)
print('One day  :', one_day)

yesterday = today - one_day
print('Yesterday:', yesterday)

tomorrow = today + one_day
print('Tomorrow :', tomorrow)