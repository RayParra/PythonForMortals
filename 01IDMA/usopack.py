#coding: utf-8
#usopack.py

from packages import fari
from packages import ftri

#print("La suma del numero {0} y el numero {1}, es: {2}".format(12, 45, fari.suma(12, 45)))

def menu(opc=0):
	if opc>=6 and opc<=9:
		x = int(input("Escribe un numero: "))
	else:
		x = int(input("Escribe un numero: "))
		y = int(input("Escribe otro numero: "))
	
	if opc==1:
		print("La suma del numero {0} y el numero {1}, es: {2}".format(x, y, fari.suma(x, y)))
	elif opc==2:
		print("La resta del numero {0} y el numero {1}, es: {2}".format(x, y, fari.resta(x, y)))
	elif opc==3:
		print("La multiplicacion del numero {0} y el numero {1}, es: {2}".format(x, y, fari.multiplicar(x, y)))
	elif opc==4:
		print("La division del numero {0} y el numero {1}, es: {2}".format(x, y, fari.dividir(x, y)))
	elif opc==5:
		print("La base del triangulo es: {0} y la altura del triangulo es: {1}, por lo tanto el area del triangulo es: {2}".format(x, y, fari.area_tri(x, y)))
	elif opc==6:
		print("El Coseno del numero: {0}, es: {1}".format(x, ftri.coseno(x)))
	elif opc==7:
		print("El Seno del numero: {0}, es: {1}".format(x, ftri.seno(x)))
	elif opc==8:
		print("La tangente del numero: {0}, es: {1}".format(x, ftri.tangente(x)))
	elif opc==9:
		print("La Raiz Cuadrada del numero: {0}, es: {1}".format(x, ftri.raiz(x)))
	elif opc==10:
		print("La potencia del numero: {0}, elvada a {1}, es: {2}".format(x, y, ftri.potencia(x, y)))
	else:
		print("Opcion no Valida!")



print("----- MENU -----")
print("")
print("1: Sumar")
print("2: Restar")
print("3: Multiplicar")
print("4: Dividir")
print("5: Area triangulo")
print("6: Coseno de X")
print("7: Seno de X")
print("8: Tangente de X")
print("9: Raiz cuadrada de X")
print("10: Potencia de X")
print("")

opc = int(input("Elige una opcion: --> "))

menu(opc)