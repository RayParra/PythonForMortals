#coding: utf-8
#areas.py
"""
Este programa es para calcular el area de figuras
geometricas, con este ejemplo podemos ilustrar
el tema de variables y constantes
"""

PI = 3.1415
r = 0.0

r = float(input("Escribe el radio del circulo: "))

print("El area del circulo es: {0: .2f}".format(pow(r, 2)*PI))