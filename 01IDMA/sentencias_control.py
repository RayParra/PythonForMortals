#coding: utf-8

#programa para explicar las sentencias de control

port = int(input("Escribe el numero de Puerto que deseas conocer: --> "))


if port == 80:
	print("El puerto numero {} es el Puerto de Internet".format(port)) # "El puerto numero " + port + "es el puerto del internt"
elif port == 23:
	print("El puerto numero {} es el Puerto de FTP".format(port)) # "El puerto numero " + port + "es el puerto del FTP"
elif port == 25:
	print("El puerto numero {} es el Puerto de SMTP(email)".format(port)) # "El puerto numero " + port + "es el puerto del SMTP(email)"
else:
	print("Puerto Desconocido")


"""
if port == 80:
	if user == 23:
		if programm == ".py":
			print("Access On")
"""

# if (port == 80) or (port == 23) or (port == 25):