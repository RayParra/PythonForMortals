#coding: utf-8
#oper_cajero.py


"""
Cuales son las Operaciones de Cajero?

1. Depositos
2. Retiros
3. Consultas

Realizar un programa para simular las operaciones basicas de un cajero de banco

"""




	
def menu(saldo):
	i = 1
	while i==1:
		
		print("CAJERO AUTOMATICO")
		print("")
		print("1. DEPOSITAR")
		print("2. RETIRAR")
		print("3. CONSULTAR")
		print("4. SALIR")
		print("")

		opc = int(input("Elige una Opcion: "))
		print("")

		if opc == 1:
			cantidad = float(input("Escribe la cantidad a depositar: $"))
			saldo += depositar(cantidad)
			print("La cantidad a depositar es: ${0}".format(cantidad))
			print("Tu saldo actual es: {0}".format(saldo))
			

		elif opc == 2:
			cantidad = float(input("Escribe la cantidad a retirar: $"))
			if saldo >= cantidad:
				saldo -= retirar(cantidad)
				print("La cantidad a retirar es: ${0}".format(cantidad))
				print("Tu saldo actual es: {0}".format(saldo))
			else:
				print("La Cantidad a Retirar excede la cantidad del saldo actual")

			

		elif opc == 3:
			print("Tu saldo actual es: ${0}".format(consultar(saldo)))
			

		elif opc == 4:
			print("GRACIAS POR UTILIZAR NUESTROS CAJEROS..")
			i = 0
		
		else:
			print("OPCION NO VALIDA")
			
	 


def depositar(deposito):
	return deposito


def retirar(retiro):
	return retiro

	


def consultar(saldo):
	return saldo


saldo = 0.0
menu(saldo)