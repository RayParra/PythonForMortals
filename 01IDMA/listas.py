#coding: utf-8
#listas.py

# programa para explicar las listas

lista_est = ["Kenia", "Uilises", "Joshua", "Edgar", "Raul", "Gustavo"]

# est1 = "Kenia"
# est2 = "Ulises"
# est3 = "Joshua"
# est4 = "Edgar"
# est5 = "Raul"
# est6 = "Gustavo"

print(lista_est)
# print(lista_est[0])
# print(lista_est[1])

# for i, est in enumerate(lista_est):
# 	print("{0}-{1}".format(i+1, est))

lista_est[5] = "Omar" # actualizar la lista (update)

lista_est.insert(6, "Gustavo")
lista_est.insert(60, "Sergio")
lista_est.insert(0, "Juan") # la funcion insert, crea nuevos registros

print(lista_est)

lista_est.append("Roberto")
lista_est.append("Argel")
print(lista_est)

print(lista_est.index("Edgar"))

if "Gilberto" in lista_est:
	print("Ya existe")
else:
	print("No existe, pero ya lo estamos agregando..")
	lista_est.append("Gilberto")
print(lista_est)
