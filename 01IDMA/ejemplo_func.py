#coding: utf-8
#ejemplo_func.py


def humano_can(edad):
	edad_can = edad * 7.0
	return edad_can

def can_humano(edad_can):
	edad_humano = edad_can / 7.0
	return edad_humano


def humano_felino(edad):
	edad_felino = edad * 6.0
	return edad_felino


def felino_humano(edad_felino):
	edad_humano = edad_felino / 6.0
	return edad_humano



def menu(opc):
	edad = int(input("Escribe la Edad: "))
	if opc == 1:
		print("La edad humana de {0}, en edad canina es: {1: 0.2f}".format(edad, humano_can(edad))) 
	elif opc == 2:
		print("La edad canina de {0}, en edad humana es: {1: 0.2f}".format(edad, can_humano(edad)))
	elif opc == 3:
		print("La edad humana de {0}, en edad felina es: {1: 0.2f}".format(edad, humano_felino(edad))) 
	elif opc == 4:
		print("La edad felina de {0}, en edad humana es: {1: 0.2f}".format(edad, felino_humano(edad)))
	else:
		print("Opcion no valida!! ")
	return 1





print("Menu:")

print("1. Humano -- Canino")
print("2. Canino -- Humano")
print("3. Humano -- Felino")
print("4. Felino -- Humano")
print("")

opc = int(input("Escribe tu Opcion: "))

menu(opc)














