#coding: utf-8
#ftri.py

import math

def coseno(x):
	return math.cos(x)

def seno(x):
	return math.seno(x)

def tangente(x):
	return math.tan(x)


def potencia(x, y):
	return math.pow(x, y)

def raiz(x):
	return math.sqrt(x)