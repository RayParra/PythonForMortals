#coding: utf-8
#oper_caj_sin_bucle.py

#oper_cajero.py


"""
Cuales son las Operaciones de Cajero?

1. Depositos
2. Retiros
3. Consultas

Realizar un programa para simular las operaciones basicas de un cajero de banco

"""




	
def menu(saldo):
	print("CAJERO AUTOMATICO")
	print("")
	print("1. DEPOSITAR")
	print("2. RETIRAR")
	print("3. CONSULTAR")
	print("4. SALIR")
	print("")

	opc = int(input("Elige una Opcion: "))
	print("")

	if opc == 1:
		cantidad = float(input("Escribe la cantidad a depositar: $"))
		saldo += depositar(cantidad)
		print("La cantidad a depositar es: ${0}".format(cantidad))
		print("Tu saldo actual es: {0}".format(saldo))
		menu(saldo)

	elif opc == 2:
		cantidad = float(input("Escribe la cantidad a retirar: $"))
		if saldo >= cantidad:
			saldo -= retirar(cantidad)
			print("La cantidad a retirar es: ${0}".format(cantidad))
			print("Tu saldo actual es: {0}".format(saldo))
		else:
			print("La Cantidad a Retirar excede la cantidad del saldo actual")

		menu(saldo)

	elif opc == 3:
		print("Tu saldo actual es: ${0}".format(consultar(saldo)))
		menu(saldo)

	elif opc == 4:
		print("GRACIAS POR UTILIZAR NUESTROS CAJEROS..")
	
	else:
		print("OPCION NO VALIDA")
		menu(saldo)
 


def depositar(deposito):
	return deposito


def retirar(retiro):
	return retiro

	


def consultar(saldo):
	return saldo


saldo = 0.0
menu(saldo)