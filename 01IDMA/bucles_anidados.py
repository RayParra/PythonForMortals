#coding: utf-8
#bucles_anidados.py

# programa para explicar los bucles anidados

# for i in range(2):
# 	print("F1")
# 	for j in range(3):
# 		print("F2")
# 		for k in range(2):
# 			print("F3")


for est in ["Raul", "Argel", "Kenia"]:
	for c in range(2):
		cal = int(input("Escribe tu calificacion!: "))
		print("Las calificaciones de {0}, son: {1}".format(est, cal))



# h = 0
# while h < 24:
# 	m = 0
# 	while m < 60:
# 		s = 0
# 		while s < 60:
# 			print("{0}:{1}:{2}".format(h, m, s))
# 			s = s + 1
# 		m = m + 1
# 	h = h + 1


# for i in range(3):
# 	print("For!")
# 	j = 0
# 	while j < 10:
# 		print("while!")
# 		j = j + 1